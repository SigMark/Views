<?php $this->title(($edit ? "Edit" : "Add") . ' Component') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0"><?= $edit ? "Edit" : "Add" ?> component</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/modules">Modules</a></li>
            <li class="breadcrumb-item"><a href="/modules/view/<?= $module->id ?>">Module <?= $module->code ?></a></li>
            <li class="breadcrumb-item active"><?= $edit ? "Edit" : "Add" ?> component</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12">
        <a href="/modules/view/<?= $module->id ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="Return"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to module</a>
    </div>
</div>

<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h3><?= $edit ? "Edit" : "Add" ?> component for module <?= $module->code ?> (<?= $module->label ?>)</h3>
                <div class="col-md-6">
                    <div class="card card-block">
                        <form class="form-horizontal" action="" method="POST">
                            <div class="form-group row">
                                <label for="inputTypeComponent" class="col-sm-3 text-right control-label col-form-label">Type <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="label" class="form-control" id="component-type" placeholder="Component" value="<?= $component->label ?>" required="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPercent" class="col-sm-3 text-right control-label col-form-label">Component type <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <select class="custom-select col-12" name="type" required="true">
                                        <option value="0" <?= ($component->type == 0) ? "selected" : "" ?>>Assessment</option>
                                        <option value="1" <?= ($component->type == 1) ? "selected" : "" ?>>Lab test</option>
                                        <option value="2" <?= ($component->type == 2) ? "selected" : "" ?>>Written exam</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputDate" class="col-sm-3 text-right control-label col-form-label">Mark percentage <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <div class="input-group bootstrap-touchspin">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary btn-outline bootstrap-touchspin-down" type="button">-</button>
                                        </span>
                                        <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                        
                                        <input id="tch3_22" type="text" name="percent" value="<?= $component->percent != 0 ? $component->percent : 33 ?>" data-bts-button-down-class="btn btn-secondary btn-outline"  required="true" data-bts-button-up-class="btn btn-secondary btn-outline" class="form-control" style="display: block;">
                                        
                                        <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary btn-outline bootstrap-touchspin-up" type="button">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputDate" class="col-sm-3 text-right control-label col-form-label">Scheduled date <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="scheduled_date" type="date" id="component-date" value="<?= date("Y-m-d", $component->scheduled_date != 0 ? strtotime($component->scheduled_date) : time()) ?>" required="true">
                                </div>
                            </div>
                            <div class="form-group m-b-0">
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
                                 </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>