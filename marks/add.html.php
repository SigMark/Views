<?php $this->title('Update Marks') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Update marks</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/modules">Modules</a></li>
            <li class="breadcrumb-item"><a href="/modules/view/">Module <?= $module->label ?></a></li>
            <li class="breadcrumb-item active">Update marks</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12">
        <a href="/modules/view/<?= $component->id_module ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="Return"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to module</a>
    </div>
</div>

<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h3>Add / Edit marks for </h3>
                <div class="table-responsive">
                    <form action="" method="POST">
                        <table class="table m-t-30 table-hover contact-list footable-loaded footable">
                            <thead>
                                <tr>
                                    <th class="footable-sortable">#<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-sortable">Username<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-sortable">First name<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-sortable">Last name<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-sortable">Mark<span class="footable-sort-indicator"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (sizeof($users)):
                                    foreach($users as $user): ?>
                                    <tr class="footable-even" style="">
                                        <td><a href="/users/view/<?= $user->id ?>"><?= $user->id ?></a></td>
                                        <td><a href="/users/view/<?= $user->id ?>">
                                            <img src="<?= $user->img_url == "" ?  "/images/avatar.png" : $user->img_url ?>" alt="user" width="120" class="img-circle"><span style="margin-left:15px;"><?= $user->username ?></span>
                                            </a>
                                        </td>
                                        <td><?= $user->firstname ?></td>
                                        <td><?= $user->lastname ?></td>
                                        <td>
                                            <?php $old = false; ?>
                                            <input type="number" min="0" max="100" name="marks[<?= $user->id ?>]" value="<?php
                                                foreach($user->marks as $mark){
                                                    if ($mark->id_component == $component->id){ //A mark exist for this component
                                                        echo $mark->point;
                                                        $old = true;
                                                    }
                                                }
                                            ?>"/>
                                            <input type="hidden" name="already[<?= $user->id ?>]" value="<?= $old ?>">
                                        </td>
                                    </tr>
                                <?php endforeach;
                                    else: ?>
                                    <tr>
                                        <td>No users for this component</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        
                            <tfoot>
                                <tr>
                                    <td colspan="9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>