<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>

<div class="row">
    <!-- Column -->
    <div class="col-md-12 col-lg-6 col-xlg-6">
        <div class="card card-inverse card-info">
            <div class="box bg-info text-center">
                <h1 class="font-light text-white"><?= $nbModules ?></h1>
                <h6 class="text-white">Modules enrolled</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-12 col-lg-6 col-xlg-6">
        <div class="card card-primary card-inverse">
            <div class="box text-center">
                <h1 class="font-light text-white"><?= $nbMarks ?></h1>
                <h6 class="text-white">Marks</h6>
            </div>
        </div>
    </div>
</div>

<h3>Modules enrolled:</h3>

<?php 
foreach($modules as $module): ?>
<div class="col-md-12">
    <div class="card card-inverse card-outline-info">
        <div class="card-header">
            <h4 class="m-b-0 text-white"><b><?= $module->label ?></b> (<?= $module->code ?>)</h4>
        </div>
        <div class="card-block">
            <div class="card card-success">
                <h4 class="m-b-0 text-white" style="padding:15px">Module info</h4>
                <div class="card-body">
                    <div class="col-md-12 row">
                <div class="col-md-6 row">
                    <label class="control-label col-md-6"><b>Module percentage success:</b></label>
                    <div class="col-md-2">
                        <h4 class="form-control-static"><b><?= $module->sum ?>%</b></h4>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6 row">
                    <label class="control-label col-md-4"><b>Module mark:</b></label>
                    <div class="col-md-4">
                                <h4 class="form-control-static">
                                    <?php if ($module->failed): ?>
                                        <span style="color:red;">Module failed</span>
                                    <?php elseif($module->sum < 50): ?>
                                        <span style="color:red;">Module failed</span>
                                    <?php elseif($module->sum >= 50 && $module->sum < 60): ?>
                                        <span style="color:orange;">Grade C</span>
                                    <?php elseif($module->sum >= 60 && $module->sum < 70): ?>
                                        <span style="color:orange;">Grade B</span>
                                    <?php elseif($module->sum >= 70 && $module->sum < 80): ?>
                                        <span style="color:green;">Grade A</span>
                                    <?php elseif($module->sum >= 80 && $module->sum < 90): ?>
                                        <span style="color:green;">Grade A+</span>
                                    <?php elseif($module->sum >= 90): ?>
                                        <span style="color:green;">Grade A++</span>
                                    <?php endif; ?>
                                </h4>
                            </div>
                </div>
            </div>
                </div>
            </div>
            <h4>Components:</h4>
            <div class="col-md-12 row">
                <?php foreach($module->components as $component): ?>
                <div class="col-md-12">
                    <div class="card card-inverse
                        <?php if($component->type == 0): ?>
                            card-outline-warning
                        <?php elseif($component->type == 1): ?>
                            card-outline-info
                        <?php else: ?>
                            card-outline-success
                        <?php endif; ?>">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"><b><?php if($component->type == 0): ?>
                                Assessment
                            <?php elseif($component->type == 1): ?>
                                Lab test
                            <?php else: ?>
                                Written exam
                            <?php endif; ?>:</b>
                            <?= $component->label ?></h4>
                        </div>
                        <div class="card-block">
                            <h3>Component percentage: <b><?= $component->hasMarks ? $component->percent . "%" : "No marks" ?></b></h3>
                            <h3>Scheduled date: <b><?= date("l d F Y", strtotime($component->scheduled_date)) ?></b></h3>
                            <h3>Mark: <b><?= $component->mark != null ? $component->mark->point : "Not evaluated" ?></b> <?php if ($component->mark != null):
                                        if ($component->mark->point > 40): ?>
                                            (<span style="color: green;">Pass</span>)
                                        <?php else: ?>
                                            (<span style="color: red;">Fail</span>)
                                        <?php endif;
                                      endif;?>
                            </h3>
                        </div>
                    </div>
                </div>
                <?php endforeach;
                    if (sizeof($module->components) == 0): ?>
                    <span style="color:black;">No components</span>
                <?php endif; ?>
            </div>
            
        </div>
    </div>
</div>
<?php 
endforeach; ?>