<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>

<div class="row">
    <!-- Column -->
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <div class="card card-inverse card-info">
            <div class="box bg-info text-center">
                <h1 class="font-light text-white"><?= $nbStudents ?></h1>
                <h6 class="text-white">Students</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <div class="card card-primary card-inverse">
            <div class="box text-center">
                <h1 class="font-light text-white"><?= $nbModules ?></h1>
                <h6 class="text-white">Modules</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-4 col-lg-4 col-xlg-4">
        <div class="card card-inverse card-success">
            <div class="box text-center">
                <h1 class="font-light text-white"><?= $nbComponents ?></h1>
                <h6 class="text-white">Components</h6>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="class-body">
            <div id="container"></div>
        </div>
    </div>
</div>

<?php echo $this->html->script([
    "plugins/highcharts"
]);?>

<script language="JavaScript">
$(document).ready(function() {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Students success by modules'
        },
        xAxis: {
            categories: ['Web Programming', 'Web Design', 'Content Managment System', 'Legal Ethical Social and Professional Issues', 'Web Developpement Framework', 'Web Technologies']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Pass',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Fail',
            data: [2, 2, 3, 2, 1]
        }]
    });
  
});
</script>