<?php $this->title(($edit ? "Edit" : "Add") . ' Module') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0"><?= $edit ? "Edit" : "Add" ?> module</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/modules">Modules</a></li>
            <li class="breadcrumb-item active"><?= $edit ? "Edit" : "Add" ?> module</li>
        </ol>
    </div>
</div>


<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h3><?= $edit ? "Edit" : "Add" ?> module</h3>
                <div class="col-md-6">
                    <div class="card card-block">
                        <form class="form-horizontal" action="" method="POST">
                            <div class="form-group row">
                                <label for="inputLabel" class="col-sm-3 text-right control-label col-form-label">Label <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputLabel" name="label" placeholder="Label" value="<?= $module->label ?>" required="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputnumber" class="col-sm-3 text-right control-label col-form-label">Code <span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputNumber" name="code" placeholder="Code" value="<?= $module->code ?>" required="true">
                                </div>
                            </div>
                           
                            <div class="form-group m-b-0">
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
                                 </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>