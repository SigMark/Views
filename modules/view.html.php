<?php $this->title('Module') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Module</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/modules">Modules</a></li>
            <li class="breadcrumb-item active">Module</li>
        </ol>
    </div>
</div>

<div class="col-12">
    <div class="card">
        <div class="card-block">
            <h4 class="card-title">Module details</h4>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3"><b>Label:</b></label>
                            <div class="col-md-9">
                                <h4 class="form-control-static"><?= $module->label ?></h4>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-3">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3"><b>Code:</b></label>
                            <div class="col-md-9">
                                <h4 class="form-control-static"><?= $module->code ?></h4>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
            </div>
            <h4>This component is enrolled by <b><?= $module->nbStudents ?></b> student<?= $module->nbStudents > 1 ? "s" : ""  ?>.</h4>
            
            <div class="col-md-12 row">
                <div class="col-md-4">
                    <a href="/modules/users/<?= $module->id ?>" class="btn btn-primary btn-pure" data-toggle="tooltip" data-original-title="Enrolled students"><i class="ti-eye" aria-hidden="true"></i> View students enrolled</a>
                    <a href="/components/add/<?= $module->id ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="Add component"><i class="ti-pencil" aria-hidden="true"></i> Add component</a>
                </div>
                <div class="col-md-4">
                    <a href="/modules/edit/<?= $module->id ?>" class="btn btn-info btn-pure" data-toggle="tooltip" data-original-title="Edit"><i class="ti-pencil" aria-hidden="true"></i> Edit module</a>
                    <a href="/modules/delete/<?= $module->id ?>" class="btn btn-danger btn-pure confirm-delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="Delete"></i> Delete module</a>
                </div> 
            </div>   
        </div>
    </div>
</div>
<div class="col-md-12">
    <h3>Components: Assessment and exams</h3>
</div>

<?php 
$sumPercent = 0;
foreach($components as $component){
    $sumPercent += $component->percent;
}
?>

<?php if ($sumPercent < 99 || $sumPercent > 100): ?>
<div class="col-md-12">
    <div class="card card-inverse card-danger">
        <div class="card-header">
            <h4 class="m-b-0 text-white">Warning</h4></div>
        <div class="card-block">
            <h3 class="card-title">Percents sum of components is <b><?= $sumPercent ?>%</b></h3>
            <p class="card-text">Please not that the sum of each component percent is not equal to 100%.</p>
        </div>
    </div>
</div>
<?php endif; ?>

<?php 
foreach($components as $component): ?>
<div class="col-md-12">
    <div class="card card-inverse
        <?php if($component->type == 0): ?>
            card-outline-warning
        <?php elseif($component->type == 1): ?>
            card-outline-info
        <?php else: ?>
            card-outline-success
        <?php endif; ?>">
        <div class="card-header">
            <h4 class="m-b-0 text-white"><b>
                <?php if($component->type == 0): ?>
                    Assessment
                <?php elseif($component->type == 1): ?>
                    Lab test
                <?php else: ?>
                    Written exam
                <?php endif; ?>:</b>
                <?= $component->label ?>
            </h4>
        </div>
        <div class="card-block">
            <div class="col-md-12 row">
                <div class="col-md-3">
                    <h3>Value percentage <b><?= $component->percent ?>%</b></h3>
                </div>
                <div class="col-md-6">
                    <h3>Scheduled date <b><?= date("l d F Y", strtotime($component->scheduled_date)) ?></b></h3>
                </div>
            </div>
            
            <p class="label label-info"><?= $component->countUserMarked ?> mark<?= $component->countUserMarked > 1 ? "s" : "" ?> setted of <?= $module->nbStudents  ?> student<?= $module->nbStudents > 1 ? "s" : ""  ?> enrolled.</p>
            <br/>
            <a href="/marks/add/<?= $component->id ?>" class="btn btn-success">Add marks</a>
            <a href="/components/edit/<?= $component->id ?>" class="btn btn-inverse">Edit</a>
            <a href="/components/delete/<?= $component->id ?>" class="btn btn-danger confirm-delete">Delete</a>
        </div>
    </div>
</div>
<?php 
endforeach; ?>

<div class="col-md-12">
    <div class="card">
        <div class="class-body">
            <div id="container"></div>
        </div>
    </div>
</div>

<?php echo $this->html->script([
    "plugins/highcharts"
]);?>

<script language="JavaScript">
$(document).ready(function() {  
   var chart = {
      type: 'bar'
   };
   var title = {
      text: 'Note in module by student'   
   };
   var subtitle = {
      text: 'Source: DB'  
   };
   var xAxis = {
      text:'Students',
      categories: ['Student1','Student2','Student3','Student4'],
      title: {
         text: null
      }
   };
   var yAxis = {
      min: 0,
      max:100,
      title: {
         text: 'Marks',
         align: 'high'
      },
      labels: {
         overflow: 'justify'
      }
   };
   var tooltip = {
      valueSuffix: '%'
   };
   var plotOptions = {
      bar: {
         dataLabels: {
            enabled: true
         }
      }
   };
   var legend = {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
   };
   var credits = {
      enabled: false
   };
   
   var series= [{
         name: 'Mark',
            data: [17,40,85,74]
        }
   ];     
      
   var json = {};   
   json.chart = chart; 
   json.title = title;   
   json.subtitle = subtitle; 
   json.tooltip = tooltip;
   json.xAxis = xAxis;
   json.yAxis = yAxis;  
   json.series = series;
   json.plotOptions = plotOptions;
   json.legend = legend;
   json.credits = credits;
   $('#container').highcharts(json);
  
});
</script>