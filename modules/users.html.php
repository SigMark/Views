<?php $this->title('Users Enrolled') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Students enrolled</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/modules">Modules</a></li>
            <li class="breadcrumb-item"><a href="/modules/view/<?= $module->id ?>">Module <?= $module->label ?></a></li>
            <li class="breadcrumb-item active">Students enrolled</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12">
        <a href="/modules/view/<?= $module->id ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="Return"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to module</a>
    </div>
</div>

<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">All students enrolled for this module</h4>
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list footable-loaded footable" data-page-size="10">
                        <thead>
                            <tr>
                                <th class="footable-sortable">#<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Username<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">First Name<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Last Name<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (sizeof($users)):
                                    foreach($users as $user): ?>
                                <tr class="footable-even" style="">
                                    <td><a href="/users/view/<?= $user["id"] ?>"><?= $user["id"] ?></a></td>
                                    <td><a href="/users/view/<?= $user["id"] ?>"><?= $user["username"] ?></a></td>
                                    <td><?= $user["firstname"] ?></td>
                                    <td><?= $user["lastname"] ?></td>
                                    <td>
                                        <a href="/users/view/<?= $user["id"] ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="View"><i class="ti-pencil" aria-hidden="true"></i> View user</a>
                                        <a href="/modules/deleteUser/<?= $module->id ?>/<?= $user["id"] ?>" class="btn btn-warning btn-pure delete-row-btn confirm-delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i> Delete form this module</a>
                                    </td>
                                </tr>
                            <?php endforeach;
                                else: ?>
                                <tr>
                                    <td>
                                        No students added for this module
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Add students</h4>
                <form action="" method="POST">
                    <div class="col-md-12 row">
                        <div class="col-md-9 row">
                            <input type="text" id="add-students" name="addStudents[]" placeholder="Add students by name">
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $this->html->style([
    "magicsuggest.min"
]);?>

<?php echo $this->html->script([
    "plugins/magicsuggest.min"
]);?>


<script>
	jQuery(document).ready(function()
	{
		jQuery('#add-students').magicSuggest({
			data: JSON.parse('<?php echo addslashes(json_encode($notEnrolledStudents)) ?>'),
			//value: JSON.parse('<?php //echo addslashes($this->catSourcesJSON) ?>'),
			allowFreeEntries : false,
			allowDuplicates : false,
			valueField: 'id',
			sortOrder: 'lastname',
			displayField: 'name',
			expandOnFocus : true,
			name: 'addStudents[]',
			maxSelection: null
		});
		
	});
</script>