<?php $this->title('Modules') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Modules</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Modules</li>
        </ol>
    </div>
</div>


<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">All modules</h4>
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list footable-loaded footable" data-page-size="10">
                        <thead>
                            <tr>
                                <th class="footable-sortable">#<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Label<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Code<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Nb components<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Nb enroled students<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (sizeof($modules)):
                                    foreach($modules as $module): ?>
                                <tr class="footable-even" style="">
                                    <td><a href="/modules/view/<?= $module->id ?>"><?= $module->id ?></a></td>
                                    <td><h3>
                                            <span class="label label-danger"><?= $module->label ?>
                                            </span>    
                                        </h3>
                                    </td>
                                    <td><a href="/modules/view/<?= $module->id ?>"><?= $module->code ?></a></td>
                                    <td><?= $module->nbComponents ?></td>
                                    <td><?= $module->nbStudents ?></td>
                                    <td>
                                        <a href="/modules/view/<?= $module->id ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="View"><i class="ti-eye" aria-hidden="true"></i> View</a>
                                        <a href="/modules/edit/<?= $module->id ?>" class="btn btn-info btn-pure" data-toggle="tooltip" data-original-title="Edit"><i class="ti-pencil" aria-hidden="true"></i> Edit</a>
                                        <a href="/modules/delete/<?= $module->id ?>" class="btn btn-danger btn-pure delete-row-btn confirm-delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i> Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach;
                                else: ?>
                                <tr>
                                    <td>
                                        No modules
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <a class="btn btn-info btn-rounded" href="/modules/add">Add new Module </a>
                                </td>
                                
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>