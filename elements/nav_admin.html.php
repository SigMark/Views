<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="<?= lithium\storage\Session::read("userImgUrl") == "" ?  "/images/avatar.png" : lithium\storage\Session::read("userImgUrl") ?>" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text">
                <a href="/profile" class="link" aria-haspopup="true" aria-expanded="true">
                    <?= lithium\storage\Session::read("userFirstName") ?> <?= lithium\storage\Session::read("userLastName") ?>
                </a>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li>
                    <a href="/" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                </li>

                <li class="nav-devider"></li>
                <li class="nav-small-cap">ADMINISTRATION</li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Modules</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/modules">List Modules</a></li>
                        <li><a href="/modules/add">New Module</a></li>
                    </ul>
                </li>
                <li>    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Users</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/users/index/teachers">Teachers</a></li>
                        <li><a href="/users/index/students">Students</a></li>
                        <li><a href="/users/add">New User</a></li>
                    </ul>
                </li>
                <li class="nav-devider"></li>
                <li class="nav-small-cap">PERSONAL</li>

                <li>
                    <a href="/profile"><i class="fa fa-list"></i><span class="hide-menu">My Profile</span></a>
                </li>
                <li class="nav-devider"></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer center">
        <a href="/logout" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->