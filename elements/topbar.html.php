<header class="topbar">
    <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <!-- Logo icon -->
                <b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="/images/logo.png" alt="homepage" class="dark-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                 <!-- dark Logo text -->
                 <img src="/images/logo-text.png" alt="homepage" class="dark-logo" /></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0 ">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
            </ul>

            <ul class="navbar-nav my-lg-0">
                <li class="nav-item">
                    <span class="nav-link">
                        <?= lithium\storage\Session::read("userFirstName") ?> <?= lithium\storage\Session::read("userLastName") ?>
                    </span>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-muted waves-effect waves-dark" href="/profile">
                        <img src="<?= lithium\storage\Session::read("userImgUrl") == "" ?  "/images/avatar.png" : lithium\storage\Session::read("userImgUrl") ?>" alt="user" class="profile-pic" />
                    </a>
                </li>
                <li class="nav-item">
                     <a class="nav-link waves-effect waves-dark" href="/logout"><i class="fa fa-power-off nav-link"></i></a>
                </li>
            </ul>
            
        </div>
    </nav>
</header>