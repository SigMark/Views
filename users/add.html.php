<?php $this->title('Add User') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Add user</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active">Add user</li>
        </ol>
    </div>
</div>


<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h3>Add student / teacher</h3>
                <div class="col-md-6">
                    <div class="card card-block">
                        <form class="form-horizontal" action="" method="POST">
                                <div class="form-group row">
                                    <label for="inputFirstName" class="col-sm-3 text-right control-label col-form-label">First name <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputFirstName" name="firstname" required="true" placeholder="Johnathan">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputLastName" class="col-sm-3 text-right control-label col-form-label">Last name <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputLastName" name="lastname" required="true" placeholder="Doe">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputUsername" class="col-sm-3 text-right control-label col-form-label">Username <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputUsername" name="username" required="true" placeholder="jdoe">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="optionGender" class="col-sm-3 text-right control-label col-form-label">Gender <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                    <select class="form-control custom-select" required="true" name="gender" required="true" data-placeholder="Choose a gender">
                                        <option value="0">Male</option>
                                        <option value="1">Female</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="optionRole" class="col-sm-3 text-right control-label col-form-label">Role <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                    <select class="form-control custom-select" required="true" name="role" required="true" data-placeholder="Choose a role">
                                        <option value="1">Teacher</option>
                                        <option value="0">Student</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputnumber" class="col-sm-3 text-right control-label col-form-label">Email <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail" name="email" required="true" placeholder="johnathan@admin.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputnumber" class="col-sm-3 text-right control-label col-form-label">Phone number</label>
                                    <div class="col-sm-9">
                                        <input type="tel" class="form-control" id="inputPhone" name="phone" placeholder="0139019411">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputUsername" class="col-sm-3 text-right control-label col-form-label">Address</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="inputAddress" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputnumber" class="col-sm-3 text-right control-label col-form-label">Password <span style="color: red;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="inputpassword" name="password" required="true">
                                    </div>
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
                                     </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>