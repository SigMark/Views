<?php $this->title('Users') ?>
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0"><?php if($student): ?>
                    Student
                <?php else: ?>
                    Teacher
                <?php endif; ?></h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">
                <?php if($student): ?>
                    All students
                <?php else: ?>
                    All teachers
                <?php endif; ?>
                </li>
        </ol>
    </div>
</div>


<div class="row">
    <div class=col-md-12>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title"><?php if($student): ?>
                    All students
                <?php else: ?>
                    All teachers
                <?php endif; ?></h4>
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list footable-loaded footable" data-page-size="10">
                        <thead>
                            <tr>
                                <th class="footable-sortable">#<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Username<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">First name<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Last name<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Gender<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Mail address<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Postal address<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Phone number<span class="footable-sort-indicator"></span></th>
                                <th class="footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (sizeof($users)):
                                foreach($users as $user): ?>
                                <tr class="footable-even" style="">
                                    <td><a href="/users/view/<?= $user->id ?>"><?= $user->id ?></a></td>
                                    <td><a href="/users/view/<?= $user->id ?>">
                                        <img src="<?= $user->img_url == "" ?  "/images/avatar.png" : $user->img_url ?>" alt="user" width="120" class="img-circle"><span style="margin-left:15px;"><?= $user->username ?></span>
                                        </a>
                                    </td>
                                    <td><?= $user->firstname ?></td>
                                    <td><?= $user->lastname ?></td>
                                    <td><?php if($user->gender == 0): ?>
                                        <span class="label label-info">Male</span>
                                        <?php else: ?>
                                        <span class="label label-primary">Female</span>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= $user->email ?></td>
                                    <td><?= $user->address ?></td>
                                    <td><?= $user->phone ?></td>
                                    <td>
                                        <a href="/users/view/<?= $user->id ?>" class="btn btn-success btn-pure" data-toggle="tooltip" data-original-title="View / Edit">View / Edit</a>
                                        <a href="/users/delete/<?= $user->id ?>" class="btn btn-danger btn-pure delete-row-btn confirm-delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i> Delete</button>
                                    </td>
                                </tr>
                            <?php endforeach;
                                else: ?>
                                <tr>
                                    <td>No users</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    
                        <tfoot>
                            <tr>
                                <td colspan="9">
                                    <a class="btn btn-info btn-rounded" href="/users/add">Add new <?= ($student) ? "student" : "teacher" ?> </a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>