<?php $this->title('Profile') ?>
<!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-block">
                <center class="m-t-30"> 
                    <img src="<?= $user->img_url == "" ?  "/images/avatar.png" : $user->img_url ?>" class="img-circle" width="150" />
                    <h4 class="card-title m-t-10"><?= $user->firstname ?> <?= $user->lastname ?></h4>
                    <h6 class="card-subtitle">@<?= $user->username ?></h6>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-block"> <small class="text-muted">Email address </small>
                <h6><?= $user->email ?></h6> 
                <?php if($user->phone != ""): ?>
                    <small class="text-muted p-t-30 db">Phone</small>
                    <h6><?= $user->phone ?></h6> 
                <?php endif;
                    if ($user->address != ""): ?>
                    <small class="text-muted p-t-30 db">Address</small>
                    <h6><?= $user->address ?></h6>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!--second tab-->
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                <br>
                                <p class="text-muted"><?= $user->firstname ?> <?= $user->lastname ?></p>
                            </div>
                            <?php if($user->phone != ""): ?>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted"><?= $user->phone ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                <br>
                                <p class="text-muted"><?= $user->email ?></p>
                            </div>
                            <div class="col-md-3 col-xs-6"> <strong>Gender</strong>
                                <br>
                                <p class="text-muted"><?= $user->gender == 0 ? "Male" : "Female" ?></p>
                            </div>
                        </div>
                        <hr>
                        
                        <h4 class="font-medium m-t-30">Skill Set</h4>
                        <hr>
                        <h5 class="m-t-30">Web Development <span class="pull-right">80%</span></h5>
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                        </div>
                        <h5 class="m-t-30">Web Design <span class="pull-right">90%</span></h5>
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                        </div>
                        <h5 class="m-t-30">Content Managment System <span class="pull-right">50%</span></h5>
                        <div class="progress">
                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                        </div>
                        <h5 class="m-t-30">Web Technologies <span class="pull-right">70%</span></h5>
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="settings" role="tabpanel">
                    <div class="card-block">
                        <form class="form-horizontal form-material" action="" method="POST">
                            <div class="form-group">
                                <label class="col-md-12">First Name <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Johnathan" name="firstname" class="form-control form-control-line" value="<?= $user->firstname ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Last Name <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Doe" name="lastname" class="form-control form-control-line" value="<?= $user->lastname ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Gender <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <select class="form-control custom-select" required="true" name="gender" required="true" data-placeholder="Choose a gender">
                                            <option value="0" <?= $user->gender == 0 ? "selected" : "" ?>>Male</option>
                                            <option value="1" <?= $user->gender == 1 ? "selected" : "" ?>>Female</option>
                                    </select>                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Username <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="jdoe" name="username" class="form-control form-control-line" value="<?= $user->username ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <input type="email" placeholder="johnathan@admin.com" name="email" class="form-control form-control-line" value="<?= $user->email ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Phone No</label>
                                <div class="col-md-12">
                                    <input type="tel" placeholder="123 456 7890" name="phone" class="form-control form-control-line" value="<?= $user->phone ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Address</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" id="inputAddress" name="address"><?= $user->address ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Password <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line">
                                </div>
                            </div>
       
                              <p>
                                  Set a new password to change the current password.
                              </p>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" type="submit">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>